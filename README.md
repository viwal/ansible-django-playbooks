# Ansible playbook to promote and deploy Python/Django webapp

### Config
Main configuration is placed in `inventory/<stage>/group_vars/<env>.yaml`

*Note: Config files are split by stage*


### Usage
`ansible-playbook -i inventory/<stage>/hosts <playbook>.yaml`